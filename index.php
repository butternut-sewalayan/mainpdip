<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="description" content="Halo, saya Dwi Purnomo. Saya senang disebut sebagai social entrepreneur yang memanfaatkan teknologi bagi banyak kegiatan pengembangan yang berdampak luas bagi masyarakat. Saya juga sering disebut sebagai design thinker.">
<meta name="keywords" content="">
<meta name="author" content="Butternut Media">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<title>Dwi Purnomo</title>
<!--
Stimulus Template
http://www.templatemo.com/tm-498-stimulus
-->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/templatemo-style.css">

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700" rel="stylesheet">

</head>
<body data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->

<!-- <div class="preloader">

     <div class="spinner">
          <p>Mohon Tunggu... </p><span class="spinner-rotate"></span>
     </div>

</div> -->


<!-- Navigation Section -->

<div class="navbar navbar-fixed-top custom-navbar" role="navigation">
     <div class="container">

          <!-- navbar header -->
          <div class="navbar-header">
               <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
               </button>
               <a href="#" class="navbar-brand">Dwi Purnomo</a>
          </div>

          <div class="collapse navbar-collapse">
               <ul class="nav navbar-nav navbar-right">
                    <li><a href="#about" class="smoothScroll">About Me</a></li>
                    <li><a href="#pendidikan" class="smoothScroll">Education</a></li>
                    <li><a href="#experiences" class="smoothScroll">Experiences</a></li>
                    <li><a href="#achievements" class="smoothScroll">Achievements</a></li>
                    <li><a href="#contact" class="smoothScroll">Contact</a></li>
               </ul>
          </div>

     </div>
</div>


<!-- Home Section -->

<section id="home" class="parallax-section">
     <div class="container">
          <div class="row">

               <div class="col-md-6 col-sm-6">
                    <div class="home-img"></div>
               </div>

               <div class="col-md-6 col-sm-6">
                    <div class="home-thumb">
                         <div class="section-title">
                              <h4 class="wow fadeInUp color-red" data-wow-delay="0.3s">Dr. Dwi Purnomo, STP., MT.</h4>
                              <h1 class="wow fadeInUp" data-wow-delay="0.6s">Hello, I'm <strong>Dwi Purnomo</strong></h1>
                              <p class="wow fadeInUp" data-wow-delay="0.9s">Saya senang disebut sebagai social entrepreneur yang memanfaatkan teknologi bagi banyak kegiatan pengembangan yang berdampak luas bagi masyarakat. Saya juga sering disebut sebagai design thinker.</p>
                              
                              <a href="#about" class="wow fadeInUp smoothScroll section-btn btn btn-success" data-wow-delay="1.4s">Siapa saya?</a>
                              
                         </div>
                    </div>
               </div>


          </div>
     </div>
</section>


<!-- About Section -->

<section id="about">
     <div class="container">
          <div class="row">

               <div class="col-md-6 col-sm-12 col-md-offset-3">
                    <div class="about-thumb">
                         <div class="wow fadeInUp section-title" data-wow-delay="0.4s">
                              <h1>I'm a design thinker &nbsp;<i class="fa fa-lightbulb-o" style="color: #bd353a"></i></h1> 
                              <h4 class="color-yellow">Design Thinking for Social Innovation</h4>
                         </div>
                         <div class="wow fadeInUp" data-wow-delay="0.8s">
                              <p>Saya telah menginisiasi banyak komunitas kreatif seperti Jatinangor Creative Forum dan melakukan aktivasi komunitas di beberapa area dan mendirikan The Local Enablers sebagai pelopor penggunaan design thinking untuk inovasi sosial di Indonesia. </p>
                              <p>Saat ini saya sedang mengakselerasi 40 startup berbasis teknologi dengan menghubungkan stakeholders dari hulu ke hilir menggunakan pendekatan design thinking.</p>
                         </div>
                    </div>
               </div>

          </div>
     </div>
</section>


<!-- Pendidikan Section -->

<section id="pendidikan" class="parallax-section">
     <div class="container">
          <div class="row">

               <div class="col-md-6 col-sm-6">
                    <div class=" pendidikan-img"></div>
               </div>

               <div class="col-md-6 col-sm-6">
                    <div class="color-white pendidikan-thumb">
                         <div class="wow fadeInUp section-title text-center" data-wow-delay="0.8s">
                              <h1>Pendidikan</h1>
                              
                         </div>

                         <div class="wow fadeInUp color-white media" data-wow-delay="0.2s">
                              <div class="media-object media-left media-top">
                                   <i class="fa fa-graduation-cap"></i>
                              </div>
                              <div class="media-body">
                                   <h3 class="media-heading">Institut Pertanian Bogor (IPB)</h3>
                                   <p class="color-white">Ph.D, Agro-Industrial Technology </p>
                                    <small>2007 – 2011</small>
                              </div>
                         </div>

                         <div class="wow fadeInUp color-white media" data-wow-delay="0.2s">
                              <div class="media-object media-left media-top">
                                   <i class="fa fa-graduation-cap"></i>
                              </div>
                              <div class="media-body">
                                   <h3 class="media-heading">Institut Teknologi Bandung (ITB) </h3>
                                   <p class="color-white">Master Degree, Industrial Engineering</p>
                                   <small>2003 – 2005</small>
                              </div>
                         </div>

                         <div class="wow fadeInUp color-white media" data-wow-delay="0.2s">
                              <div class="media-object media-left media-top">
                                   <i class="fa fa-graduation-cap"></i>
                              </div>
                              <div class="media-body">
                                   <h3 class="media-heading">Universitas Padjadjaran (Unpad)</h3>
                                   <p class="color-white">Bachelor, Agricultural Engineering
                                   </p><!-- <p class="color-dark">
                                   Vice Chairman of Agriculture Engineering Student Association</p> -->
                                   <small>1998 – 2003</small>
                              </div>
                         </div>

                         <div class="wow fadeInUp color-white media" data-wow-delay="0.2s">
                              <div class="media-object media-left media-top">
                                   <i class="fa fa-university"></i>
                              </div>
                              <div class="media-body">
                                   <h3 class="media-heading">SMUN 8 Bandung </h3>
                                   <p class="color-white">High School, Physical Sciences</p>
                                   <small>1995 – 1998</small>
                              </div>
                         </div>

                    </div>
               </div>

               

          </div>
     </div>
</section>



<!-- Experiences Section -->

<section id="experiences" class="parallax-section">
     <div class="container">
          <div class="row">
               <div class="wow fadeInUp section-title text-center" data-wow-delay="0.4s">
                    <h1>My Experiences</h1> 
               </div>
          </div>
          <div class="row nopadding ">
               <div class="col-md-6 col-sm-12 ">
                    <div class="experiences-thumb">
                         
                         <div class="wow fadeInUp" data-wow-delay="0.8s">
                              <ul>
                                   <li><b>Unpad-Ibu Popon Collaboration; Best Practice in Sustainable Assistance Model for Social Entrepreneurship in Agroindustrial based SME's</b><p>The 2014 International Conference on Agroindustry (ICOA): Competitive and sustainable Agroindustry for Human Welfare</p></li>
                                   <li><b>Science-Based Curriculum (Technoprenership) Development in Agriculture Industry Technology Study Program, Universitas Padjadjaran</b><p>National Conference on Innovation and Technopreneurship</p></li>
                                   <li><b>Jatinangor Creative Hub; The Empirical-based Ecosystem Creation Model in Creating  New Entrepreneurs</b><p>National Conference and Technopreneurship</p></li>
                                   <li><b>Social Enterprise Concept in Sustaining Poultry-Based Agroindustry Development in Indonesia Case Study Muscovy Beard Social Business</b><p>International Conference of Agroindustry</p></li>
                                   <li><b>Design Thinking Approach In Cipageran Dairy Products Initiation. Case Study of Your-good, The Winner of West Java New Entrepreneur 2016</b><p>National Seminar on Agroindustry Technology Professionals Association</p></li>
                                   <li><b>Social Entrepreneurship Replication Method In Increasing Motivation on  Local Commodity based Entrepreneurial Community in Banyuresmi Garut</b><p>National Seminar on Agroindustrial Technology Professionals Association</p></li>
                              </ul>
                         </div>
                    </div>
               </div>

               <div class="col-md-6 col-sm-12 ">
                    <div class="experiences-thumb">
                         
                         <div class="wow fadeInUp" data-wow-delay="0.8s">
                              <ul>
                                   <li><b>The Local Enablers;  Carrying  Local Oriented Empowerment in the Global Social Business Framework</b><p>National Conference in Tecnopreneurship</p></li>
                                   <li><b>Design Thinking in Agroindustrial-Based Approach Social Enterprise Development</b><p>International Conference of Business Administration in 2016 ICBA</p></li>
                                   <li><b>Utilization of Zakat Infak Shodaqoh Model for Developing Social Entrepreneurship</b><p>National Seminar on Optimization Zakat In Community Empowerment</p></li>
                                   <li><b>“1000 Shoes Movement” Designing Sustainable Kindness Chain Model in Social Activities</b><p> National Seminar on Optimization Zakat In Community Empowerment</p></li>
                                   <li><b>Biomass Development in Bangai - Central Sulawesi Sea using Design Thinking Approach</b><p>International Conference On Biomass</p></li>
                                   <li><b>Social Enterprise Concept in Sustaining Fruit-Processing Agro-Based Industry Develop-ment. Case Study in Indonesia Fruits Up Social Business</b><p>International Conference of Agroindustry</li></p></li>
                              </ul>
                         </div>
                    </div>
               </div>

          </div>
     </div>
</section>




<!-- Achievement Section -->

<section id="achievements" class="parallax-section">
     <div class="container">
          <div class="row">
               
               <div class="col-md-6 col-sm-12 col-md-offset-6">
                    <div class="achievements-img"></div>
               </div>

               <div class="col-md-6 col-sm-12 ">
                    <div class="color-white achievements-thumb">
                         <div class="wow fadeInUp section-title text-center" data-wow-delay="0.8s">
                              <h1>Achievement</h1>
                              
                         </div>

                         <div class="wow fadeInUp color-white media" data-wow-delay="0.2s">
                              <div class="media-object media-right media-middle">
                                   <i class="fa fa-trophy"></i>
                              </div>
                              <div class="media-body">
                                   <p class="color-white">
                                        2014 Innovation and Initiative Award (food category) from The Governor of West Java Province 
                                   </p>
                              </div>
                         </div>

                         <div class="wow fadeInUp color-white media" data-wow-delay="0.4s">
                              <div class="media-object media-right media-middle">
                                   <i class="fa fa-trophy"></i>
                              </div>
                              <div class="media-body">
                                   <p class="color-white">
                                        2015 The Outstanding Young Person of the Year award from the Junior Chamber International Bandung 
                                   </p>
                              </div>
                         </div>

                         <div class="wow fadeInUp color-white media" data-wow-delay="0.4s">
                              <div class="media-object media-right media-middle">
                                   <i class="fa fa-trophy"></i>
                              </div>
                              <div class="media-body">
                                   <p class="color-white">
                                        2015 The National Entrepreneurship Activator Award from Ministry of Youth Republic of Indonesia
                                   </p>
                              </div>
                         </div>

                         <div class="wow fadeInUp color-white media" data-wow-delay="0.4s">
                              <div class="media-object media-right media-middle">
                                   <i class="fa fa-trophy"></i>
                              </div>
                              <div class="media-body">
                                   <p class="color-white">
                                        2016 West Java Province Award for New Start-Up Business in Social Business (Yourgood)
                                   </p>
                              </div>
                         </div>

                         <div class="wow fadeInUp color-white media" data-wow-delay="0.4s">
                              <div class="media-object media-right media-middle">
                                   <i class="fa fa-trophy"></i>
                              </div>
                              <div class="media-body">
                                   <p class="color-white">
                                        2017 West Java Province Award for New Start-Up Business in Education & Empower-ment  (Sacitamuda)
                                   </p>
                              </div>
                         </div>
                    </div>
               </div>

                

          </div>
     </div>
</section>

<!-- Quotes Section -->

<section id="quotes">
     <div class="overlay"></div>
     <div class="container">
          <div class="row">

               <div class="col-md-offset-1 col-md-10 col-sm-12">
                    <i class="wow fadeInUp fa fa-quote-right" data-wow-delay="0.6s"></i>
                    <div class="wow fadeInUp tweets-container" id="tweets-container" data-wow-delay="0.8s">
                         <p>Memuat quotes dari Twitter..</p>

                    </div>
                    #dipquotes &mdash;<i class="fa fa-twitter " style="font-size: 2rem;"></i> <a href="http://twitter.com/dwiindrapurnomo/">@dwiindrapurnomo</a>
                    
               </div>

          </div>
     </div>
</section>



<!-- Contact Section -->

<section id="contact" class="parallax-section">
     <div class="container">
          <div class="row">

               <div class="bg-dark col-md-6 col-sm-12">
                    <div class="contact-form">
                         <div class="wow fadeInUp section-title" data-wow-delay="0.2s">
                              <h1 class="color-white">Kontak Saya</h1>
                              <p class="color-white">Hubungi saya via media sosial atau melalui form di bawah ini: </p>
                         </div>

                         <div id="contact-form">
                              <form name="sentMessage" id="contactForm" novalidate>
                                   <div class="wow fadeInUp" data-wow-delay="1s">
                                        <input name="name" type="text" class="form-control" id="name" placeholder="Nama Anda" required data-validation-required-message="Silakan isi nama Anda.">
                                   </div>
                                   <div class="wow fadeInUp" data-wow-delay="1.2s">
                                        <input name="email" type="email" class="form-control" id="email" placeholder="Email Anda..." required data-validation-required-message="Silakan isi email Anda">
                                   </div>
                                  <!--  <div class="wow fadeInUp" data-wow-delay="1.4s">
                                        <input name="sub"  class="form-control" id="message" placeholder="Judul Pesan Anda"></input>
                                   </div> -->
                                   <div class="wow fadeInUp" data-wow-delay="1.6s">
                                        <textarea name="message" rows="5" class="form-control" id="message" placeholder="Tulis pesan Anda..." required data-validation-required-message="Silakan isi pesan Anda"></textarea>
                                   </div>
                                   <div id="success"></div>
                                   <div class="wow fadeInUp col-md-6 col-sm-8" data-wow-delay="1.8s">
                                        <button name="submit" type="submit" class="btn btn-x2" id="send">Kirim Pesan</button>
                                   </div>
                              </form>

                         </div>

                    </div>
               </div>

               

               <div class="bg-dark col-md-6 col-sm-12">
                    <div class="contact-thumb">
                         <div class="wow fadeInUp contact-info" data-wow-delay="0.6s">
                              <h3 class="color-white">Temui saya di: </h3>
                              <p><b>Rumah Kolaborasi</b> <br>
                              Komplek Puri Indah Blok A6 No 14C, Jalan Kolonel Ahmad Syam, Sayang, Jatinangor, Sumedang</p>

                         </div>

                         <div class="wow fadeInUp contact-info" data-wow-delay="0.8s">
                              <h3 class="color-white">Kontak dan Media Sosial</h3>
                              <p><i class="fa fa-envelope-o"></i> <a href="mailto:dwi.purnomo@unpad.ac.id">dwi.purnomo@unpad.ac.id</a></p>
                              <p><i class="fa fa-facebook"></i><a href="https://www.facebook.com/dwiindrapurnomo">Dwi Indra Purnomo</a></p>
                              <p><i class="fa fa-instagram"></i><a href="http://instagram.com/dwiindrapurnomo/">@dwiindrapurnomo</a></p><p><i class="fa fa-twitter"></i><a href="http://twitter.com/dwiindrapurnomo/">@dwiindrapurnomo</a></p>
                         </div>

                    </div>
               </div>

          </div>
     </div>
</section>


<!-- Footer Section -->

<footer>
     <div class="row">
          Dwi Purnomo - &copy; 2017
     </div>
</footer>

<!-- SCRIPTS -->

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.parallax.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jqBootstrapValidation.js"></script>
<script src="js/tweets.js"></script>
<script src="js/contact.js"></script>
<script src="js/custom.js"></script>

</body>
</html>